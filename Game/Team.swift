//
//  Team.swift
//  Axe
//
//  Created by Neal Watkins on 2021/8/13.
//

import Foundation

struct Team: Codable {
    
    var name: String
}
