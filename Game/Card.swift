//
//  Card.swift
//  Axe
//
//  Created by Neal Watkins on 2021/8/13.
//

import Foundation

struct Card: Codable {
    
    var question: String
    
    init(_ question: String) {
        self.question = question
    }
    
}
