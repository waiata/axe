//
//  Game.swift
//  Axe
//
//  Created by Neal Watkins on 2021/8/13.
//

import Foundation

class Game: Codable {
    
    var deck = Deck()
    var cast = Cast()
    
    var currentQuestion: String {
        return deck.current?.question ?? ""
    }
}


extension Game {
    
    static var sample: Game {
        let game = Game()
        game.deck.cards = Deck.sampleCards
        game.deck.next()
        return game
    }
    
}
