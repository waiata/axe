//
//  Deck.swift
//  Axe
//
//  Created by Neal Watkins on 2021/8/13.
//

import Foundation

class Deck: Codable {
    
    var cards = [Card]()
    
    var kitty = [Card]()
    
    var current: Card?
    
    func next() {
        if cards.isEmpty { return }
        if kitty.isEmpty { deal() }
        current = kitty.removeFirst()
    }
    
    func deal() {
        kitty = cards
        if Default.shuffle {
            kitty.shuffle()
        }
    }
    
    func add(question: String) {
        let card = Card(question)
        cards.append(card)
        kitty.append(card)
    }
 
    struct Default {
        static var shuffle: Bool {
            return UserDefaults.standard.bool(forKey: "Shuffle Deck")
        }
    }
}

extension Deck {
    static let sampleCards: [Card] = [
        Card("Question 1"),
        Card("Question 2"),
        Card("Question 3"),
        Card("Question 4"),
        Card("Question 5"),
        Card("Question 6")
    ]
}
