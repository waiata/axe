//
//  QuestionView.swift
//  Axe
//
//  Created by Neal Watkins on 2021/8/13.
//

import SwiftUI

struct QuestionView: View {
    
    var question: String
    
    var body: some View {
        Text(question)
            .font(.largeTitle)
            .fontWeight(.bold)
            .padding(.all)
    }
}

struct QuestionView_Previews: PreviewProvider {
    static var previews: some View {
        QuestionView(question: "Where's My Phone")
    }
}
