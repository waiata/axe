//
//  FocusedValues.swift
//  Axe
//
//  Created by Neal Watkins on 2021/8/13.
//

import SwiftUI

extension FocusedValues {
    
    var game: Binding<Game>? {
        get { self[FocusedGameKey.self] }
        set { self[FocusedGameKey.self] = newValue }
    }
    
    private struct FocusedGameKey: FocusedValueKey {
        typealias Value = Binding<Game>
    }
}
