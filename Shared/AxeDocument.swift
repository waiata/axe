//
//  AxeDocument.swift
//  Shared
//
//  Created by Neal Watkins on 2021/8/13.
//

import SwiftUI
import UniformTypeIdentifiers


struct AxeDocument: FileDocument {
    
    var game = Game()
    
    init() {
        
    }

    init(configuration: ReadConfiguration) throws {
        guard let data = configuration.file.regularFileContents
        else {
            throw CocoaError(.fileReadCorruptFile)
        }
        self.game = try JSONDecoder().decode(Game.self, from: data)
    }
    
    func fileWrapper(configuration: WriteConfiguration) throws -> FileWrapper {
        let data = try JSONEncoder().encode(game)
        return .init(regularFileWithContents: data)
    }
    
    static var readableContentTypes: [UTType] { [.axe] }

}

extension UTType {
    static var axe: UTType {
        UTType(importedAs: "net.waiata.axe")
    }
}
