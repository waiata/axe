//
//  AxeApp.swift
//  Shared
//
//  Created by Neal Watkins on 2021/8/13.
//

import SwiftUI

@main
struct AxeApp: App {
    
    init() {
        registerDefaults()
    }
    
    func registerDefaults() {
        let plist = Bundle.main.path(forResource: "Defaults", ofType: "plist")
        if let dic = NSDictionary(contentsOfFile: plist!) as? [String: Any] {
            UserDefaults.standard.register(defaults: dic)
        }
    }
    
    var body: some Scene {
        DocumentGroup(newDocument: AxeDocument()) { file in
            GameView(game: file.$document.game)
        }
        .commands {
            GameMenu()
        }
    }
    
    
}
