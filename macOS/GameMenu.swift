//
//  GameMenu.swift
//  Axe
//
//  Created by Neal Watkins on 2021/8/13.
//

import SwiftUI

struct GameMenu: Commands {
    
    @FocusedBinding(\.game) private var garden: Game?
    
    var body: some Commands {
        CommandMenu("Game") {
            Button("Next Question") {
                garden?.deck.next()
            }
        }
    }
}

