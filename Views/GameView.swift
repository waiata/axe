//
//  GameView.swift
//  Axe
//
//  Created by Neal Watkins on 2021/8/13.
//

import SwiftUI

struct GameView: View {
    
    
    @Binding var game: Game
    
    var body: some View {
        TabView {
            DeckView(game: $game)
                .tabItem {
                    Text("Deck")
                    Image(systemName: "questionmark.square")
                }
            CastView(game: $game)
                .tabItem {
                    Text("Cast")
                    Image(systemName: "person.2")
                }
            PlayView(game: $game)
                .tabItem {
                    Text("Play")
                    Image(systemName: "play")
                }
        }.padding()
    }
}

struct GameView_Previews: PreviewProvider {
    static var previews: some View {
        GameView(game: .constant(Game.sample))
    }
}
