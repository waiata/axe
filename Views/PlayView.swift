//
//  PlayView.swift
//  Shared
//
//  Created by Neal Watkins on 2021/8/13.
//

import SwiftUI

struct PlayView: View {
    
    @Binding var game: Game
    
    var body: some View {
        
        VStack {
            QuestionView(question: game.currentQuestion)
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .toolbar {
            Toolbar(game: game)
        }
    }
    
    struct Toolbar: ToolbarContent {
        var game: Game
        @State private var popQuestion: Bool = true
        
        var body: some ToolbarContent {
            ToolbarItem() {
                Button("Next") { game.deck.next() }
            }
            
        }
    }
}

struct PlayView_Previews: PreviewProvider {
    static var previews: some View {
        PlayView(game: .constant(Game.sample))
    }
}
