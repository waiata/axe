//
//  PlayView.swift
//  Shared
//
//  Created by Neal Watkins on 2021/8/13.
//

import SwiftUI

struct DeckView: View {
    
    @Binding var game: Game
    
    var body: some View {
        
        Text("Deck")
    }
    
}

struct DeckView_Previews: PreviewProvider {
    static var previews: some View {
        DeckView(game: .constant(Game.sample))
    }
}
