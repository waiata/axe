//
//  CastView.swift
//  Shared
//
//  Created by Neal Watkins on 2021/8/13.
//

import SwiftUI

struct CastView: View {
    
    @Binding var game: Game
    
    var body: some View {
        
        Text("Cast")
    }
    
    struct Toolbar: ToolbarContent {
        var game: Game
        
        var body: some ToolbarContent {
            ToolbarItem() {
                Button("Next") { game.deck.next() }
            }
           
        }
    }
}

struct CastView_Previews: PreviewProvider {
    static var previews: some View {
        CastView(game: .constant(Game.sample))
    }
}
